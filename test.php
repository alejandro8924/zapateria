<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
	</head>
	<body>
		<input type="text" id="mesanje">
		<button id="btn">enviar</button>
		<script src="js/jquery.min.js"></script>
		<script type="text/javascript" src="http://localhost:3000/socket.io/socket.io.js"></script>
		<script type="text/javascript">
		var socket = io.connect('localhost:3000');

		$('#btn').click(function() {
			socket.emit('send_message', {
				user: 'Rafael Moreno',
				image: 'http://finaldechiste.com/wp-content/uploads/2012/12/foto-perfil.jpg',
				message: $('#mesanje').val()
			});
			$('#mesanje').val('')
		});

		$('#mesanje').keypress(function(event) {

			if ($(this).val() != "") {
				socket.emit('typing_send', {
					sender: 2,
	        		reciever: 1,
					typ: 1
				});
			} else {
				socket.emit('typing_send', {
					sender: 2,
	        		reciever: 1,
					typ: 0
				});
			};

			if (event.keyCode == 13) {
				if ($(this).val() != "") {
					socket.emit('send_message', {
						sender: 2,
	            		reciever: 1,
						message: $(this).val()
					});
					$(this).val('');
				};
			};
		});
		</script>
	</body>
</html>