function toggleMobileMenu() {
    var $mobileMenu = $('#mobile-main-menu');
    $mobileMenu.slideToggle('fast');
}

$('#mobile-menu-button').on('click', toggleMobileMenu);