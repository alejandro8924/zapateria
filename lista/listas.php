<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once '../administrador/lib/Autoload.php';

$p = new Productos();
$m = new Marcas();

$productos = json_decode($p->products_list());
$marcas = json_decode($m->brands_List());
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Zapateria</title>
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/general.css">
		<link rel="stylesheet" type="text/css" href="../css/products_list.css">
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="../css/fonts.css">
	</head>
	<body>
		<header id="main-header">
			<?php include "../inc/main-header.php"; ?>
		</header>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div id="filter">
						<h4 id="marcas" class="filter-options">Marcas</h4>
						<ul>
							<?php for ($i=0; $i < count($marcas); $i++) { ?>
							<li><a href="<?php echo $marcas[$i]->marca; ?>"><?php echo $marcas[$i]->marca; ?></a></li>
							<?php } ?>
						</ul>
						<h4 id="categorias" class="filter-options">Categorías</h4>
						<ul>
							<?php for ($i=0; $i < count($categorias); $i++) { ?>
							<li><a href="<?php echo $categorias[$i]->categoria; ?>"><?php echo $categorias[$i]->categoria; ?></a></li>
							<?php } ?>
						</ul>
						<h4 id="precio" class="filter-options">Precio</h4>
						<ul>
							<li><a href="#">menor a 8000 Bs.</a></li>
							<li><a href="#">mayor a 8000 Bs.</a></li>
							<li><a href="#">menor a 8000 Bs.</a></li>
							<li><a href="#">mayor a 8000 Bs.</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-9">
					<div id="main-banner">
						<div class="row">
							<div class="col-md-12">
								<div id="banner">
									<div class="row">
										<div class="col-sm-6 hidden-xs">
											<img src="../img/PR-ASUS-Eee-Pad-Transformer-Prime-docked-Champagne-Gold.jpg" alt="" class="img-responsive">
										</div>
										<div class="col-sm-6">
											<div class="row">
												<div class="col-xs-8">
													<img src="../img/asus_logo.jpg" alt="" class="img-responsive">
												</div>
											</div>
											<h4> Eee Pad Transformer Prime</h4>
											<p>Windows 8.1 atau versi lain Tipis dan Ringan, dengan prosesor Intel® quad-core Dock keyboard yang nyaman dengan layar IPS Daya tahan baterai hingga 11 jam untuk komputasi seharian</p>
											<a href="#" class="btn-more">Mas</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<section id="articles">
						<div class="row">
							<?php $cant = 12; ?>
							<?php if (count($productos) < 12) { $cant = count($productos); } ?>
							<?php for ($i=0; $i < count($productos); $i++) { ?>
							<article class="col-sm-4">
								<div class="product">
									<span class="price"><?php echo $productos[$i]->costo1; ?> Bs.</span>
									<img src="../administrador/photos/<?php echo $productos[$i]->url; ?>/imagen1.jpg" alt="" class="img-responsive">
									<div class="description">
										<h4><?php echo $productos[$i]->descripcion; ?></h4>
										<p><?php echo $productos[$i]->descripcion_larga; ?></p>
									</div>
								</div>
								<a href="../producto/<?php echo $productos[$i]->id; ?>/<?php echo $productos[$i]->url; ?>" class="btn-more in-product">Mas detalles</a>
							</article>
							<?php } ?>
						</div>
					</section>
				</div>
			</div>
		</div>
		<?php include '../inc/footer.php'; ?>
		<script type="text/javascript" src="../js/jquery.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../js/scripts.js"></script>
	</body>
</html>