<?php
session_start();
require_once 'Config/FacebookAccessToken.php';
require_once 'facebook/vendor/autoload.php';

$fb = new Facebook\Facebook([
	'app_id' => $app_id,
	'app_secret' => $app_secret,
	'default_graph_version' => $default_graph_version,
	]);

$helper = $fb->getRedirectLoginHelper();
$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes', 'publish_actions']; // optional
$loginUrl = $helper->getLoginUrl('http://' . $_SERVER['HTTP_HOST'] . '/zapateria/administrador/facebook/login-callback.php', $permissions);
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Login | Administrador</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/iCheck/square/blue.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="../../index2.html"><b>Admin</b>LTE</a>
			</div>
			<div class="login-box-body">
				<p class="login-box-msg">Ingresa tus datos</p>
				<form id="formLogin" action="php/login.php" method="POST">
					<div class="form-group has-feedback">
						<input type="email" name="email" id="email" class="form-control" autofocus placeholder="Correo Electrónico">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="password" id="password" class="form-control" placeholder="Cotraseña">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-8">
							<div class="checkbox icheck">
								<label>
									<input type="checkbox"> Recordarme
								</label>
							</div>
						</div>
						<div class="col-xs-4">
							<button id="loginBtn" type="button" class="btn btn-primary btn-block btn-flat">Entrar</button>
						</div>
					</div>
				</form>
				<div class="social-auth-links text-center">
					<p>- O -</p>
					<a href="<?php echo $loginUrl; ?>" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Entrar con Facebook</a>
					<!--<a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Entrar con Google +</a>-->
				</div>
				<div id="alert">
					
				</div>
			</div>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/iCheck/icheck.min.js"></script>
		<script type="text/javascript" src="dist/js/pages/access.js"></script>
		<script type="text/javascript">
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});
		});
		</script>
	</body>
</html>