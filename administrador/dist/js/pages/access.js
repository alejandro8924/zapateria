var login = function() {
    $.ajax({
        url: 'php/login.php',
        type: 'POST',
        dataType: 'json',
        data: {
            email: $('#email').val(),
            password: $('#password').val()
        },
        beforeSend: function() {
            console.log("Cargando...");
        },
        success: function(data) {
            console.log(data);
            if (data.status == 0) {
                $('#email').val('').focus();
                $('#password').val('');
                $('#alert').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban"></i> Error!</h4>' + data.description + '.</div>');
            } else if (data.status == 1) {
                window.location = "http://" + window.location.hostname + "/zapateria/administrador/principal.php";
            };
        },
        timeout: 4000,
        error: function(err) {
            console.log(err);
        }
    });
}

$('#loginBtn').click(login);

$('#formLogin').keyup(function(event) {
    if (event.keyCode == 13) {
    	login();
    };
});