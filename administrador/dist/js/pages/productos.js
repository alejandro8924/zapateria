$('input[type="file"]').on('change', function() {
    if ($(this).val() !== "") {
        var id = $(this).attr('id');
        var archivos = document.getElementById(id).files;
        var navegador = window.URL || window.webkitURL;

        var objeto_url = navegador.createObjectURL(archivos[0]);
        $(this).parent().css({
            'background-image': 'url("' + objeto_url + '")',
            'background-size': 'cover',
            'background-repeat': 'no-repeat',
            'background-position': '0 50%'
        });
    } else {
        $(this).parent().removeAttr('style');
    };
});

$('#descripcion').keyup(function() {
    $.ajax({
        url: 'php/create_url.php',
        type: 'POST',
        dataType: 'html',
        data: {
            descripcion: $('#descripcion').val()
        },
        beforeSend: function() {

        },
        success: function(data) {
            $('#url').val(data);
        },
        timeout: 4000,
        error: function(err) {
            console.log(err);
        }
    });
});

$('#agregar').click(function() {
    var formData = new FormData($('#formulario')[0]);
    $.ajax({
        url: 'php/add_producto.php',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function() {
            console.log("Cargando...");
            $('#alerta').html('<div class="callout callout-info"><h4>Analizando!</h4><p>Verificando datos y publicando</p></div>');
            $('#agregar').addClass('disabled').text('Publicando producto');
        },
        success: function(data) {
            console.log(data);
            if (data.status == 0) {
                $('#alerta').html('<div class="callout callout-danger"><h4>Error!</h4><p>' + data.description + '</p></div>');
                $('#agregar').removeClass('disabled').text('Agregar producto');
            } else if (data.status == 1) {
                $('input, select, textarea').val('');
                $('#alerta').html('<div class="callout callout-success"><h4>Exito!</h4><p>' + data.description + '</p></div>');
            };
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
            $('#alerta').html('<div class="callout callout-danger"><h4>Error ' + err.status + '!</h4><p>' + err.statusText + '</p></div>');
        }
    });
});

/*
$('#colores').change(function() {
    console.log($(this).val());
});
*/