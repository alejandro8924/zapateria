var socket = io.connect('localhost:3000');

var misocketid = null;

var reciever = null;

socket.on('info', function(data) {
    console.log(data);
    misocketid = data;

    socket.emit('data_user', {
        id: misocketid,
        user: 'Rafael Moreno'
    });
});


socket.on('contactos', function(data) {
    console.log(data);
    for (var i = 0; i < data.length; i++) {
        $('.contacts-list').append('<li data-id="' + data[i].id + '"><a href="#"><img class="contacts-list-img" src="../dist/img/user1-128x128.jpg"><div class="contacts-list-info"><span class="contacts-list-name">' + data[i].id + ' <small class="contacts-list-date pull-right">2/28/2015</small></span><span class="contacts-list-msg">How have you been? I was...</span></div></a></li>');
    };
});

socket.on('contacto', function(data) {
    console.log(data);
    $('.contacts-list').append('<li data-id="' + data.id + '"><a href="#"><img class="contacts-list-img" src="../dist/img/user1-128x128.jpg"><div class="contacts-list-info"><span class="contacts-list-name">' + data.id + ' <small class="contacts-list-date pull-right">2/28/2015</small></span><span class="contacts-list-msg">How have you been? I was...</span></div></a></li>');
});


//<span id="num_messages" data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span>

var num_messages = 0;
socket.on('receive_message', function(data) {
    num_messages = num_messages + 1;

    //box box-primary direct-chat direct-chat-primary collapsed-box

    if ($('.direct-chat').hasClass('collapsed-box')) {
        $('.box-header').css({
            'color': '#fff',
            'background-color': '#3C8DBC'
        });
    };

    $('.direct-chat-messages').animate({
        scrollTop: $('.direct-chat-messages').prop('scrollHeight')
    }, 1000);

    var msj = data.message;

    $('#num_messages').text(num_messages).attr('title', num_messages + ' New Messages');
    $('.direct-chat-messages').append('<div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">' + data.user + '</span><span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span></div><img class="direct-chat-img" src="' + data.image + '" alt="message user image"><div class="direct-chat-text"></div></div>');
    $('.direct-chat-text').last().text(data.message);
});

/*
$('#send-message').click(function() {
    socket.emit('send_message', {
        user: 'Rafael Moreno',
        image: 'http://finaldechiste.com/wp-content/uploads/2012/12/foto-perfil.jpg',
        message: $('#mesanje').val()
    });
    $('#mesanje').val('')
});
*/

$('#mesanje').keypress(function(event) {

    if (event.keyCode == 13) {

        $('.direct-chat-messages').append('<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">Sarah Bullock</span><span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span></div><img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><div class="direct-chat-text">' + $(this).val() + '</div></div>');

        $('.direct-chat-messages').animate({
            scrollTop: $('.direct-chat-messages').prop('scrollHeight')
        }, 1000);

        socket.emit('send_message', {
            sender: misocketid,
            reciever: reciever,
            message: $('#mesanje').val()
        });
        $('#mesanje').val('')
    };
});

$('#mesanje').focus(function() {
    num_messages = 0;
    $('#num_messages').text(num_messages).attr('title', num_messages + ' New Messages');
});

$('#min-plus').click(function() {
    $('.box-header').removeAttr('style');
    num_messages = 0;
    $('#num_messages').text(num_messages).attr('title', num_messages + ' New Messages');
});


$(".direct-chat-messages, .contacts-list").slimscroll({
    height: $(this).navbarMenuHeight,
    alwaysVisible: false,
    size: $(this).navbarMenuSlimscrollWidth
});


$('.contacts-list').on('click', 'li', function() {
	reciever = $(this).data('id');
    var box = $(this).parents('.direct-chat').first();
    box.toggleClass('direct-chat-contacts-open');
});
/*
$(document).on('click', o.directChat.contactToggleSelector, function() {
    var box = $(this).parents('.direct-chat').first();
    box.toggleClass('direct-chat-contacts-open');
});
*/
socket.on('typing_recieve', function(data) {
    if (data.typ == 1) {
        $('#typing').text('Escribiendo...');
    } else {
        $('#typing').text('');
    };
});