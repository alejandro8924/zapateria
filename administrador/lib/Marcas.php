<?php
require_once 'Connection.php';
/**
* 
*/
class Marcas extends Connection {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	/*CRUD*/
	public function read($id_marca) {
		$json = array();
		$query = "SELECT * FROM `marca` WHERE `id` = ?";
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param("i", $id_marca);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $marca);
			$stmt->fetch();

			$json = array(
				"status" => 1,
				"id" => $id,
				"marca" => $marca
				);
		} else {
			$json = array(
				"status" => 0,
				"description" => "error"
				);
		}

		$stmt->close();
		return json_encode($json);
	}
	/*CRUD*/
	
	public function brands_List() {
		$json = array();
		$query = "SELECT * FROM `marcas`";
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $marca);
			while ($stmt->fetch()) {
				array_push($json, array(
					"id" => $id,
					"marca" => $marca
					));
			}
		}
		$stmt->close();
		return json_encode($json);
	}
}
?>