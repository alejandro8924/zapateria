<?php
function parseLabel($string) {
	$array = explode("|", $string);
	$cad = "";
	for ($i=0; $i < count($array); $i++) { 
		$array[$i] = '<span class="label label-primary">' . $array[$i] . '</span> ';
		$cad = $cad . $array[$i];
	}
	return $cad;
}

function status($valor) {
	if ($valor == 1) {
		return '<span class="label label-success">Activo</span>';
	} else {
		return '<span class="label label-danger">Inactivo</span>';
	}
}

function stock($cantidad) {
	if ($cantidad <= 10) {
		return '<span class="label label-danger">' . $cantidad . '</span>';
	} else {
		return '<span class="label label-success">' . $cantidad . '</span>';
	}
}

function if_var_null($var) {
	if (empty($var)) {
		$var = NULL;
	}
	return $var;
}

function upload_image($file, $path) {
	if ($file['type'] != "image/jpg" || $file['type'] != "image/jpeg") {
			
		if (!file_exists($path)) {
			mkdir($path);
		}

		$image = 1;
		while (file_exists($path . '/imagen' . $image . '.jpg')) {
			$image = $image + 1;
		}

		move_uploaded_file($file['tmp_name'], $path . '/imagen' . $image . '.jpg');
	}
}
?>