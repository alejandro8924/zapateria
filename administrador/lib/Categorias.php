<?php
require_once 'Connection.php';
/**
* 
*/
class Categorias extends Connection {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	/*CRUD*/
	public function read($id_categoria) {
		$json = array();
		$query = "SELECT * FROM `categorias` WHERE `id` = ?";
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param("i", $id_categoria);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $categoria);
			$stmt->fetch();

			$json = array(
				"id" => $id,
				"categoria" => $categoria
				);
		} else {
			$json = array(
				"status" => 0,
				"description" => "error"
				);
		}

		$stmt->close();
		return json_encode($json);
	}
	/*CRUD*/

	public function category_List() {
		$json = array();
		$query = "SELECT * FROM `categorias`";
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $categoria);
			while ($stmt->fetch()) {
				array_push($json, array(
					"id" => $id,
					"categoria" => $categoria
					));
			}
		}
		$stmt->close();
		return json_encode($json);
	}
}
?>