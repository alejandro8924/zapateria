<?php
require_once 'Connection.php';
/**
* 
*/
class Access extends Connection {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	public function login($email, $password = NULL) {
		$json = array();
		$email = htmlspecialchars($email, ENT_QUOTES);
		$query = "SELECT `nombre`, `apellido`, `correo`, `clave`, `foto` FROM `usuarios` WHERE `correo` = ?";
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param("s", $email);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($nombre, $apellido, $correo, $clave, $foto);
			$stmt->fetch();
			if ($correo == $email) {
				if (!is_null($password)) {
					$password = md5($password);
					if ($clave == $password) {
						session_start();
						$_SESSION['name'] = $nombre . " " . $apellido;
						$_SESSION['email'] = $correo;
						$_SESSION['foto'] = $foto;
						$json = array(
							"status" => 1,
							"description" => "login correcto"
							);
					} else {
						$json = array(
							"status" => 0,
							"description" => "error en los datos"
							);
					}
				} else {
					session_start();
					$_SESSION['name'] = $nombre . " " . $apellido;
					$json = array(
						"status" => 1,
						"description" => "login correcto"
						);
				}
			} else {
				$json = array(
					"status" => 0,
					"description" => "error en los datos"
					);
			}
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}

		$stmt->close();
		return json_encode($json);
	}
}
?>