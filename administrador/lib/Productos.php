<?php
require_once 'Connection.php';
/**
* 
*/
class Productos extends Connection {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	public static function create_url($name) {
		$name = trim($name);
	 
	    $name = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $name
	    );
	 
	    $name = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $name
	    );
	 
	    $name = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $name
	    );
	 
	    $name = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $name
	    );
	 
	    $name = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $name
	    );

		$url = strtolower($name);
		$url = str_replace(" ", "-", $url);

	    return $url;
	}

	/*CRUD*/
	public function create($descripcion, $descripcion_larga, $p_claves, $marca, $categoria, $colores, $costo1, $costo2, $costo3, $estado) {
		$query = "INSERT INTO `productos` VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$url = self::create_url($descripcion);
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param("ssssiisdddi", $descripcion, $descripcion_larga, $p_claves, $url, $marca, $categoria, $colores, $costo1, $costo2, $costo3, $estado);
			$stmt->execute();
			return $this->mysqli->insert_id;
		} else {
			return NULL;
		}
		$stmt->close();
	}

	public function read($id_producto) {
		$json = array();
		$tallas = array();
		$query = "SELECT * FROM `productos` WHERE `id` = ?";
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param("i", $id_producto);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $descripcion, $descripcion_larga, $p_claves, $url, $marca, $categoria, $colores, $costo1, $costo2, $costo3, $estado);
			$stmt->fetch();

			$query2 = "SELECT * FROM `tallas` WHERE `producto` = ?";
			if ($stmt2 = $this->mysqli->prepare($query2)) {
				$stmt2->bind_param("i", $id_producto);
				$stmt2->execute();
				$stmt2->store_result();
				$stmt2->bind_result($id_talla, $producto, $talla, $cantidad);
				while ($stmt2->fetch()) {
					@array_push($talla, array(
						"id" => $id_talla,
						"talla" => $talla,
						"cantidad" => $cantidad
						));
				}
			}

			$json = array(
				"status" => 1,
				"id" => $id,
				"descripcion" => $descripcion,
				"descripcion_larga" => $descripcion_larga,
				"p_claves" => $p_claves,
				"url" => $url,
				"marca" => $marca,
				"categoria" => $categoria,
				"tallas" => $tallas,
				"colores" => $colores,
				"costo1" => $costo1,
				"costo2" => $costo2,
				"costo3" => $costo3,
				"estado" => $estado
				);
		} else {
			$json = array(
				"status" => 0,
				"description" => "error"
				);
		}

		$stmt->close();
		return json_encode($json);
	}

	public function update($id, $descripcion, $url, $marca, $categoria, $numero, $colores, $costo1, $costo2, $costo3, $cantidad, $estado) {
		$query = "UPDATE `productos` SET `descripcion` = ?, `url` = ?, `marca` = ?, `categoria` = ?, `numero` = ?, `colores` = ?, `costo1` = ?, `costo2` = ?, `costo3` = ?, `cantidad` = ?, `estado` = ? WHERE `id` = ?";
		$url = self::create_url($descripcion);
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param("ssiissdddiii", $descripcion, $url, $marca, $categoria, $numero, $colores, $costo1, $costo2, $costo3, $cantidad, $estado, $id);
			$stmt->execute();
			return true;
		} else {
			return false;
		}
		$stmt->close();	
	}

	public function delete($id) {
		$query = "DELETE FROM `productos` WHERE `id` = ?";
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param("i", $id);
			$stmt->execute();
			return true;
		} else {
			return false;
		}
		$stmt->close();	
	}
	/*CRUD*/

	public function products_list() {
		$json = array();
		$query = "SELECT * FROM `productos`";
		if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $descripcion, $descripcion_larga, $p_claves, $url, $marca, $categoria, $colores, $costo1, $costo2, $costo3, $estado);

			while ($stmt->fetch()) {
				
				$query2 = "SELECT * FROM `tallas` WHERE `producto` = ?";
				if ($stmt2 = $this->mysqli->prepare($query2)) {
					$stmt2->bind_param("i", $id);
					$stmt2->execute();
					$stmt2->store_result();
					$stmt2->bind_result($id_talla, $producto, $talla, $cantidad);
					$tallas = array();
					while ($stmt2->fetch()) {
						array_push($tallas, array(
							"id" => $id_talla,
							"talla" => $talla,
							"cantidad" => $cantidad
							));
					}
				}

				array_push($json, array(
					"id" => $id,
					"descripcion" => $descripcion,
					"descripcion_larga" => $descripcion_larga,
					"p_claves" => $p_claves,
					"url" => $url,
					"marca" => $marca,
					"categoria" => $categoria,
					"tallas" => $tallas,
					"colores" => $colores,
					"costo1" => $costo1,
					"costo2" => $costo2,
					"costo3" => $costo3,
					"estado" => $estado
					));
			}
		} else {
			$json = array(
				"status" => 0,
				"description" => "error"
				);
		}

		$stmt->close();
		return json_encode($json);

	}
}
?>