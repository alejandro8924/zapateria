<?php
if (!empty($_POST['email']) && !empty($_POST['password'])) {
	require_once '../lib/Access.php';
	$login = new Access();
	echo $login->login($_POST['email'], $_POST['password']);
} else {
	$json = array(
		"status" => 0,
		"description" => "Los campos están vacíos"
		);

	echo json_encode($json);
}
?>