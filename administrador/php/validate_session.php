<?php
$ruta = NULL;
if (file_exists("index.php")) {
	$ruta = "";
} elseif (file_exists("../index.php")) {
	$ruta = "../";
}

session_start();
if (empty($_SESSION['email']) || empty($_SESSION['name'])) {
	session_destroy();
	header("Location: http://localhost/zapateria/administrador/index.php");
}
?>