<?php
require_once '../php/validate_session.php';
require_once '../lib/Autoload.php';


$cat = new Categorias();
$mar = new Marcas();

$marcas = json_decode($mar->brands_List());
$categorias = json_decode($cat->category_List());
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>AdminLTE 2 | Productos</title>
		<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="../plugins/iCheck/all.css">
		<link rel="stylesheet" type="text/css" href="../plugins/select2/select2.min.css">
		<link rel="stylesheet" type="text/css" href="../dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="../dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" type="text/css" href="../dist/css/styles.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include "../inc/main-header.php"; ?>
			</header>
			<aside class="main-sidebar">
				<?php include "../inc/main-sidebar.php"; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Productos <small>Agregar Producto</small></h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo $ruta; ?>principal.php"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Productos</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Quick Example</h3>
								</div>
								<form id="formulario" action="php/add_producto.php" enctype="multipart/form-data" autocomplete="off">
									<div class="box-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="descripcion">Descripción</label>
													<input type="text" id="descripcion" class="form-control" name="descripcion" placeholder="Ejm: Converse all star">
												</div>
												<div class="form-group">
													<label for="url">Enlace del producto</label>
													<input type="url" id="url" class="form-control" name="url" readonly>
												</div>
												<div class="form-group">
													<label for="marca">Marca</label>
													<select name="marca" id="marca" class="form-control select2" style="width: 100%;">
														<option selected="selected">Seleccione:</option>
														<?php for ($i=0; $i < count($marcas); $i++) { ?>
														<option value="<?php echo $marcas[$i]->id; ?>"><?php echo $marcas[$i]->marca; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="form-group">
													<label for="categoria">Categoría</label>
													<select name="categoria" id="categoria" class="form-control select2" style="width: 100%;">
														<option value="">Seleccione:</option>
														<?php for ($i=0; $i < count($categorias); $i++) { ?>
														<option value="<?php echo $categorias[$i]->id; ?>"><?php echo $categorias[$i]->categoria; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="form-group">
													<label for="colores">Colores</label>
													<select name="colores[]" id="colores" class="form-control select2" multiple="multiple" style="width: 100%;">
														<option value="blanco">blanco</option>
														<option value="negro">negro</option>
														<option value="rojo">rojo</option>
														<option value="azul">azul</option>
													</select>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-4">
															<label for="costo1">Costo 1</label>
															<div class="input-group">
																<input type="text" id="costo1" class="form-control" name="costo1">
																<span class="input-group-addon">Bs.</span>
															</div>
														</div>
														<div class="col-md-4">
															<label for="costo2">Costo 2</label>
															<div class="input-group">
																<input type="text" id="costo2" class="form-control" name="costo2">
																<span class="input-group-addon">Bs.</span>
															</div>
														</div>
														<div class="col-md-4">
															<label for="costo3">Costo 3</label>
															<div class="input-group">
																<input type="text" id="costo3" class="form-control" name="costo3">
																<span class="input-group-addon">Bs.</span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label for="descripcion_larga">Descripción Larga</label>
													<textarea name="descripcion_larga" id="descripcion_larga" class="form-control"></textarea>
												</div>
												<div class="form-group">
													<label for="p_claves">Palabras Clave</label>
													<input type="text" id="p_claves" class="form-control" name="p_claves" placeholder="Ejm:">
												</div>
												<div class="form-group">
													<label>
														<input type="checkbox" id="publicar_facebook" class="minimal" name="publicar_facebook" checked>
														publicar en Facebook
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12">
														<div id="content-imagen1" class="upload">
															<input type="file" id="imagen1" name="imagen1">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div id="content-imagen2" class="upload">
															<input type="file" id="imagen2" name="imagen2">
														</div>
													</div>
													<div class="col-md-6">
														<div id="content-imagen3" class="upload">
															<input type="file" id="imagen3" name="imagen3">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6 col-sm-offset-3">
													<div id="alerta">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-sm-offset-4">
													<button id="agregar" type="button" class="btn btn-block btn-primary btn-flat">Agregar producto</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include "../inc/main-footer.php"; ?>
			</footer>
			<div id="chats">
				<?php include "../inc/main-chats.php"; ?>
			</div>
		</div>
		<script type="text/javascript" src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../plugins/select2/select2.full.min.js"></script>
		<script type="text/javascript" src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="../plugins/iCheck/icheck.min.js"></script>
		<script type="text/javascript" src="../plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="../dist/js/app.js"></script>
		<script type="text/javascript" src="../dist/js/pages/productos.js"></script>
		<script type="text/javascript" src="http://localhost:3000/socket.io/socket.io.js"></script>
		<script type="text/javascript" src="../dist/js/chats.js"></script>
		<script type="text/javascript">
		$(function() {
			$('.select2').select2();
			$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				checkboxClass: 'icheckbox_minimal-blue',
				radioClass: 'iradio_minimal-blue'
			});
		});
		</script>
	</body>
</html>