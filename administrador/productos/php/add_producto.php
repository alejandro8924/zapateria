<?php
session_start();
require_once '../../Config/FacebookAccessToken.php';
require_once '../../facebook/vendor/autoload.php';

$fb = new Facebook\Facebook([
	'app_id' => $app_id,
	'app_secret' => $app_secret,
	'default_graph_version' => $default_graph_version,
	]);

$json = array();

if (!empty($_POST['descripcion']) && !empty($_POST['url']) && !empty($_POST['marca']) && !empty($_POST['categoria']) && !empty($_POST['colores']) && !empty($_POST['costo1']) && !empty($_POST['descripcion_larga']) && !empty($_POST['p_claves']) && isset($_FILES['imagen1'])) {
	require_once '../../lib/Autoload.php';
	
	$add = new Productos();
	$colores = implode("|", $_POST['colores']);
	$id_producto = $add->create($_POST['descripcion'], $_POST['descripcion_larga'], $_POST['p_claves'], $_POST['marca'], $_POST['categoria'], $colores, $_POST['costo1'], $_POST['costo2'], $_POST['costo3'], 1);
	
	if (!is_null($id_producto)) {
		
		upload_image($_FILES['imagen1'], '../../photos/' . $_POST['url']);
		
		if (isset($_FILES['imagen2'])) {
			upload_image($_FILES['imagen2'], '../../photos/' . $_POST['url']);
		}
		if (isset($_FILES['imagen3'])) {
			upload_image($_FILES['imagen3'], '../../photos/' . $_POST['url']);
		}

		if (!empty($_POST['publicar_facebook']) && $_POST['publicar_facebook'] == "on") {
			
			$linkData = [
				'link' => 'http://' . $_SERVER['HTTP_HOST'] . '/zapateria/producto/' . $id_producto . '/' . $_POST['url'],
				'message' => $_POST['descripcion'],
			];

			try {
				// Returns a `Facebook\FacebookResponse` object
				$response = $fb->post('/me/feed', $linkData, $_SESSION['facebook_access_token']);
			} catch (Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			$graphNode = $response->getGraphNode();

			$json = array(
				"status" => 1,
				"id_producto" => $id_producto,
				"description" => "El producto se publico satisfactoriamente tanto en la tienda online como en facebook",
				"id_pub_facebook" => $graphNode['id']
				);
		} else {
			$json = array(
				"status" => 1,
				"id_producto" => $id_producto,
				"description" => "El producto se publico satisfactoriamente",
				"id_pub_facebook" => NULL
				);
		}
	} else {
		$json = array(
			"status" => 0,
			"description" => "No se pudo publicar el producto",
			);
	}
} else {
	$json = array(
		"status" => 0,
		"description" => "Los campos están vacios",
		);

}

echo json_encode($json);
?>