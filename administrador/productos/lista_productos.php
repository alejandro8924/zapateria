<?php
require_once '../php/validate_session.php';
require_once '../lib/functions.php';
require_once '../lib/Autoload.php';

$p = new Productos();

$productos = json_decode($p->products_list());
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>AdminLTE 2 | Productos</title>
		<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="../plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="../dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" type="text/css" href="../dist/css/styles.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include "../inc/main-header.php"; ?>
			</header>
			<aside class="main-sidebar">
				<?php include "../inc/main-sidebar.php"; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Productos <small>Lista de Productos</small></h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo $ruta; ?>principal.php"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Productos</li>
					</ol>
				</section>
				<section class="content">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title"><!--Data Table With Full Features--></h3>
						</div>
						<div class="box-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>#</th>
										<th>Descripción</th>
										<th>url</th>
										<th>Marca</th>
										<th>Categoria</th>
										<th>Tallas</th>
										<th>Colores</th>
										<th>costo 1</th>
										<th>Costo 2</th>
										<th>Costo 3</th>
										<th>Cantidad</th>
										<th>Estado</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i=0; $i < count($productos); $i++) { ?>
									<tr>
										<td><?php echo $productos[$i]->id; ?></td>
										<td><?php echo $productos[$i]->descripcion; ?></td>
										<td><a href="http://localhost/zapateria/producto/<?php echo $productos[$i]->id; ?>/<?php echo $productos[$i]->url; ?>" target="_blank"><?php echo $productos[$i]->url; ?></a></td>
										<td><?php echo $productos[$i]->marca; ?></td>
										<td><?php echo $productos[$i]->categoria; ?></td>
										<?php $cantidad = 0; ?>
										<?php $tallas = ""; ?>
										<?php for ($j=0; $j < count($productos[$i]->tallas); $j++) { ?>
										<?php $cantidad = $cantidad + $productos[$i]->tallas[$j]->cantidad; ?>
										<?php $tallas = $tallas . $productos[$i]->tallas[$j]->talla . "|"; ?>
										<?php } ?>
										<td><?php echo parseLabel($tallas); ?></td>
										<td><?php echo parseLabel($productos[$i]->colores); ?></td>
										<td><?php echo number_format($productos[$i]->costo1, 2, ',', '.'); ?></td>
										<td><?php echo number_format($productos[$i]->costo2, 2, ',', '.'); ?></td>
										<td><?php echo number_format($productos[$i]->costo3, 2, ',', '.'); ?></td>
										<td><?php echo stock($cantidad); ?></td>
										<td><?php echo status($productos[$i]->estado); ?></td>
									</tr>
									<?php } ?>
								</tbody>
								<tfoot>
								<tr>
									<th>#</th>
									<th>Descripción</th>
									<th>url</th>
									<th>Marca</th>
									<th>Categoria</th>
									<th>Tallas</th>
									<th>Colores</th>
									<th>costo 1</th>
									<th>Costo 2</th>
									<th>Costo 3</th>
									<th>Cantidad</th>
									<th>Estado</th>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include "../inc/main-footer.php"; ?>
			</footer>
			<div id="chats">
				<?php include "../inc/main-chats.php"; ?>
			</div>
		</div>
		<script type="text/javascript" src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="../plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="../dist/js/app.js"></script>
		<script type="text/javascript" src="http://localhost:3000/socket.io/socket.io.js"></script>
		<script type="text/javascript" src="../dist/js/chats.js"></script>
		<script type="text/javascript">
		$(function() {
			$('#example1').DataTable();
		});
		</script>
	</body>
</html>