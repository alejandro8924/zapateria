<?php
session_start();
require_once '../Config/FacebookAccessToken.php';
require_once 'vendor/autoload.php';
require_once '../lib/Access.php';

$fb = new Facebook\Facebook([
	'app_id' => $app_id,
	'app_secret' => $app_secret,
	'default_graph_version' => $default_graph_version,
	]);

$helper = $fb->getRedirectLoginHelper();

try {
	$accessToken = $helper->getAccessToken();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
	// When Graph returns an error
	echo 'Graph returned an error: ' . $e->getMessage();
	exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
	// When validation fails or other local issues
	echo 'Facebook SDK returned an error: ' . $e->getMessage();
	exit;
}

if (isset($accessToken)) {
	// Logged in!
	$_SESSION['facebook_access_token'] = (string) $accessToken;
	// Now you can redirect to another page and use the
	// access token from $_SESSION['facebook_access_token']


	$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);

	try {
		$response = $fb->get('/me?fields=id,name,email');
		$userNode = $response->getDecodedBody();
	} catch (Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		echo 'Graph returned an error: ' . $e->getMessage();
		exit;
	} catch (Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
	}

	//$_SESSION['name'] = $userNode['name'];
	$_SESSION['email'] = $userNode['email'];
	$_SESSION['foto'] = "http://graph.facebook.com/" . $userNode['id'] . "/picture?type=large";

	$login = new Access();
	$login->login($userNode['email']);

	header("Location: http://" . $_SERVER['HTTP_HOST'] . "/zapateria/administrador/principal.php");
}
?>