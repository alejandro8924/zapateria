<?php
$ruta = NULL;
if (file_exists("index.php")) {
	$ruta = "";
	$url_ruta = "";
} elseif (file_exists("../index.php")) {
	$ruta = "../";
	$url_ruta = "../";
} 

if ($_SERVER['PHP_SELF'] == "/zapateria/producto/producto.php") {
	$url_ruta = "../../";
}

require_once $ruta . 'administrador/lib/Autoload.php';

$c = new Categorias();
$categorias = json_decode($c->category_List());

$cant = 7;
if (count($categorias) <= 7) {
	$cant = count($categorias);
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2">
			
		</div>
		<div class="col-md-10">
			<ul id="main-menu" class="nav nav-pills hidden-xs">
				<li role="presentation"><a href="<?php echo $url_ruta; ?>">Home</a></li>
				<?php for ($i=0; $i < $cant; $i++) { ?>
				<li role="presentation"><a href="<?php echo $url_ruta; ?>lista/<?php echo $categorias[$i]->categoria; ?>"><?php echo $categorias[$i]->categoria; ?></a></li>
				<?php } ?>
				<li role="presentation" class="logued"><a href="#">Entrar</a></li>
				<li role="presentation" class="logued"><a href="#">Registrate</a></li>
			</ul>
			<a href="#" id="mobile-menu-button" class="visible-xs">
				<span class="icon-menu"></span>
			</a>
		</div>
	</div>
</div>
<div class="visible-xs">
	<ul id="mobile-main-menu">
		<li role="presentation"><a href="<?php echo $url_ruta; ?>">Home</a></li>
		<?php for ($i=0; $i < $cant; $i++) { ?>
		<li role="presentation"><a href="<?php echo $url_ruta; ?>lista/<?php echo $categorias[$i]->categoria; ?>"><?php echo $categorias[$i]->categoria; ?></a></li>
		<?php } ?>
		<li role="presentation"><a href="#">Entrar</a></li>
		<li role="presentation"><a href="#">Registrate</a></li>
	</ul>
</div>