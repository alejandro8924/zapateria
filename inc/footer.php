<?php
$ruta = NULL;
if (file_exists("index.php")) {
	$ruta = "";
	$url_ruta = "";
} elseif (file_exists("../index.php")) {
	$ruta = "../";
	$url_ruta = "../";
} 

if ($_SERVER['PHP_SELF'] == "/zapateria/producto/producto.php") {
	$url_ruta = "../../";
}

require_once $ruta . 'administrador/lib/Autoload.php';

$c = new Categorias();
$categorias = json_decode($c->category_List());

$cant = 7;
if (count($categorias) <= 7) {
	$cant = count($categorias);
}
?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-2">
			</div>
			<div class="col-md-3">
				<h4>Nosotros</h4>
				<ul>
					<li><a href="#">Acerca de nosotros</a></li>
					<li><a href="#">Políticas y Privacidad</a></li>
					<li><a href="#">Términos y Condiciones</a></li>
					<li><a href="#">Contáctanos</a></li>
					<li><a href="#">Ayuda y Soporte</a></li>
				</ul>
			</div>
			<div class="col-md-2">
				<h4>Categorías</h4>
				<ul>
					<?php for ($i=0; $i < $cant; $i++) { ?>
					<li><a href="<?php echo $url_ruta; ?>lista/<?php echo $categorias[$i]->categoria; ?>"><?php echo $categorias[$i]->categoria; ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="col-md-2">
				<h4>Redes Sociales</h4>
				<ul>
					<li><a href="#" target="_blank">Facebook</a></li>
					<li><a href="#" target="_blank">Twitter</a></li>
					<li><a href="#" target="_blank">Instagram</a></li>
				</ul>
			</div>
			<div class="col-md-2">
			</div>
		</div>
	</div>
</footer>
<div id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<p>Copyright 2016</p>
			</div>
			<div class="col-md-2">
				<p>Redes Sociales</p>
			</div>
		</div>
	</div>
</div>