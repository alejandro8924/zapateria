<?php
require_once '../administrador/lib/Autoload.php';
$p = new Productos();
$producto = json_decode($p->read($_GET['id']));
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta name="description" content="<?php echo $producto->descripcion_larga; ?>">
		<meta name="keywords" content="<?php echo $producto->p_claves; ?>">
		<!-- Google Authorship y Marcado de Anunciante -->
		<!--<link rel="author" href="https://plus.google.com/118183666531546660901/posts" />
		<link rel="publisher" href="https://plus.google.com/118183666531546660901" />-->
		<!-- Schema.org markup for Google+ -->
		<meta itemprop="name" content="<?php echo $producto->descripcion; ?>">
		<meta itemprop="description" content="<?php echo $producto->descripcion_larga; ?>">
		<meta itemprop="image" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>/zapateria/administrador/photos/<?php echo $producto->url; ?>/imagen1.jpg">
		<!-- Open Graph data -->
		<!--<meta property="fb:page_id" content="487244828128560" />-->
		<meta property='og:locale' content='es_ES'/>
		<meta property='og:type' content='article'/>
		<meta property='og:title' content='<?php echo $producto->descripcion; ?>'/>
		<meta property='og:description' content='<?php echo $producto->descripcion_larga; ?>'/>
		<meta property='og:url' content='http://<?php echo $_SERVER['HTTP_HOST']; ?>/zapateria/producto/<?php echo $_GET['id']; ?>/<?php echo $producto->url; ?>'/>
		<meta property='og:site_name' content='<?php echo $producto->descripcion; ?>'/>
		<!--<meta property='fb:admins' content='100003981826956'/>-->
		<meta property='og:image' content='http://<?php echo $_SERVER['HTTP_HOST']; ?>/zapateria/administrador/photos/<?php echo $producto->url; ?>/imagen1.jpg'/>
		<!-- Twitter Card data -->
		<meta property="twitter:card" content="summary_large_image" />
		<meta property="twitter:url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>/zapateria/producto/<?php echo $_GET['id']; ?>/<?php echo $producto->url; ?>" />
		<meta property="twitter:title" content="<?php echo $producto->descripcion; ?>" />
		<meta name="twitter:description" content="<?php echo $producto->descripcion_larga; ?>">
		<meta name="twitter:image" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>/zapateria/administrador/photos/<?php echo $producto->url; ?>/imagen1.jpg">
		<!--<meta name="twitter:site" content="@sk_talleres">-->
		<title><?php echo $producto->descripcion; ?></title>
		<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../../css/general.css">
		<link rel="stylesheet" type="text/css" href="../../css/product_details.css">
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="../../css/fonts.css">
	</head>
	<body>
		<header id="main-header">
			<?php include "../inc/main-header.php"; ?>
		</header>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-5">
					<div class="img-principal">
						<img src="../../administrador/photos/<?php echo $producto->url; ?>/imagen1.jpg" alt="" class="img-responsive">
					</div>
					<div class="row">
						<div class="col-xs-6 col-sm-3">
							<div class="img-others">
								<img src="../../administrador/photos/<?php echo $producto->url; ?>/imagen1.jpg" alt="" class="img-responsive">
							</div>
						</div>
						<div class="col-xs-6 col-sm-3">
							<div class="img-others">
								<img src="../../administrador/photos/<?php echo $producto->url; ?>/imagen1.jpg" alt="" class="img-responsive">
							</div>
						</div>
						<div class="col-xs-6 col-sm-3">
							<div class="img-others">
								<img src="../../administrador/photos/<?php echo $producto->url; ?>/imagen1.jpg" alt="" class="img-responsive">
							</div>
						</div>
						<div class="col-xs-6 col-sm-3">
							<div class="img-others">
								<img src="../../administrador/photos/<?php echo $producto->url; ?>/imagen1.jpg" alt="" class="img-responsive">
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-7">
					<div class="descripcion">
						<h2><?php echo $producto->descripcion; ?></h2>
						<p><?php echo $producto->descripcion_larga; ?></p>
					</div>
					<!--<div id="fb-root"></div>-->
					<div class="fb-like" data-href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/zapateria/producto/<?php echo $_GET['id']; ?>/<?php echo $producto->url; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
					<br />
					<br />
					<div class="fb-save" data-uri="http://<?php echo $_SERVER['HTTP_HOST']; ?>/zapateria/producto/<?php echo $_GET['id']; ?>/<?php echo $producto->url; ?>" data-size="large"></div>
				</div>
			</div>
		</div>
		<?php include '../inc/footer.php'; ?>
		<script type="text/javascript" src="../../js/jquery.min.js"></script>
		<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../../js/facebook.js"></script>
		<script type="text/javascript" src="../../js/scripts.js"></script>
	</body>
</html>