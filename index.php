<?php
require_once 'administrador/lib/Autoload.php';
$p = new Productos();
$productos = json_decode($p->products_list());
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Zapateria</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/general.css">
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
	</head>
	<body>
		<header id="main-header">
			<?php include "inc/main-header.php"; ?>
		</header>
		<div id="main-banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div id="banner">
							<div class="row">
								<div class="col-sm-7 hidden-xs">
									<img src="img/banner.png" alt="" class="img-responsive">
								</div>
								<div class="col-md-4">
									<h2>Light and Slim</h2>
									<h4>The new 11-inch MacBook Air</h4>
									<p>Plus Flash storage in MacBook Air is now up to 45 per cent faster than the previous generation. So everything you do is snappier and more responsive. MacBook Air even wakes up faster than ever, thanks to flash storage and the latest Intel Core processors.</p>
	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="title">
			<h2>Lo mas nuevo</h2>
		</div>
		<section id="best-seller">
			<div class="container">
				<div class="row">
					<article class="col-sm-3">
						<div class="product">
							<img src="img/transformer_t300e_1160-100052648-orig.png"alt="" class="img-responsive">
							<div class="description">
								<h4>Samsung Galaxy S-6</h4>
								<ul>
									<li>5.2-inch display 2k</li>
									<li>32/64 Memory</li>
									<li>4Gb ram</li>
									<li>64bit exynos S processor 4 running at 2,7Ghz and 4 with 2,3Ghz</li>
								</ul>
							</div>
						</div>
						<a href="#" class="btn-more in-product">Mas detalles</a>
					</article>
					<article class="col-sm-3">
						<div class="product">
							<img src="img/transformer_t300e_1160-100052648-orig.png"alt="" class="img-responsive">
							<div class="description">
								<h4>Samsung Galaxy S-6</h4>
								<ul>
									<li>5.2-inch display 2k</li>
									<li>32/64 Memory</li>
									<li>4Gb ram</li>
									<li>64bit exynos S processor 4 running at 2,7Ghz and 4 with 2,3Ghz</li>
								</ul>
							</div>
						</div>
						<a href="#" class="btn-more in-product">Mas detalles</a>
					</article>
					<article class="col-sm-3">
						<div class="product">
							<img src="img/transformer_t300e_1160-100052648-orig.png"alt="" class="img-responsive">
							<div class="description">
								<h4>Samsung Galaxy S-6</h4>
								<ul>
									<li>5.2-inch display 2k</li>
									<li>32/64 Memory</li>
									<li>4Gb ram</li>
									<li>64bit exynos S processor 4 running at 2,7Ghz and 4 with 2,3Ghz</li>
								</ul>
							</div>
						</div>
						<a href="#" class="btn-more in-product">Mas detalles</a>
					</article>
					<article class="col-sm-3">
						<div class="product">
							<img src="img/transformer_t300e_1160-100052648-orig.png"alt="" class="img-responsive">
							<div class="description">
								<h4>Samsung Galaxy S-6</h4>
								<ul>
									<li>5.2-inch display 2k</li>
									<li>32/64 Memory</li>
									<li>4Gb ram</li>
									<li>64bit exynos S processor 4 running at 2,7Ghz and 4 with 2,3Ghz</li>
								</ul>
							</div>
						</div>
						<a href="#" class="btn-more in-product">Mas detalles</a>
					</article>
				</div>
			</div>
		</section>
		<div class="title">
			<h2>Lo mas vendido</h2>
		</div>
		<section id="articles">
			<div class="container">
				<div class="row">
					<?php $cant = 12; ?>
					<?php if (count($productos) < 12) { $cant = count($productos); } ?>
					<?php for ($i=0; $i < count($productos); $i++) { ?>
					<article class="col-sm-3">
						<div class="product">
							<span class="price"><?php echo $productos[$i]->costo1; ?> Bs.</span>
							<img src="administrador/photos/<?php echo $productos[$i]->url; ?>/imagen1.jpg" alt="" class="img-responsive">
							<div class="description">
								<h4><?php echo $productos[$i]->descripcion; ?></h4>
								<p><?php echo $productos[$i]->descripcion_larga; ?></p>
							</div>
						</div>
						<a href="producto/<?php echo $productos[$i]->id; ?>/<?php echo $productos[$i]->url; ?>" class="btn-more in-product">Mas detalles</a>
					</article>
					<?php } ?>
				</div>
			</div>
		</section>
		<div class="title">
			<h2>Promociones</h2>
		</div>
		<div class="promotions">
			<div class="container">
				
			</div>
		</div>
		<?php include 'inc/footer.php'; ?>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
	</body>
</html>